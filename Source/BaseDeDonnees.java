/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *  Classe Base de Données 
 *      Permet d'établir le lien avec la liason à la base de données
 * @author Valentin DUSSERVAIX
 * @version 1.0
 */ 
public class BaseDeDonnees {
    /**
    *Le Lien vers la table de la base peut ainsi être récupéré dans les différente fonction
    * 
    * @see Statement
    * 
    * @since 1.0
    *     
    */
    private Statement link;
    /**
    *Le Lien vers la table de la base peut ainsi être récupéré dans les différente fonction
    * 
    * @see Statement
    * 
    * @since 1.0
    *     
    */
    private Statement link2;
    /**
     * La verification de la connection à la base de données peut ainsi être récupéré
     * 
     * @see boolean
     * 
     * @since 1.0
     */
    private boolean co;
    /**
     * Essaye une connection à la base de données, change co en true si elle parvient sinon en false
     * 
     * 
     * = 
     * @see Connection
     * @see DriverManager#getConnection(java.lang.String) 
     * @see Connection#createStatement()
     * @see BaseDeDonnees#co
     * @see BaseDeDonnees#link
     * 
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     * @throws java.sql.SQLException
     * @since 1.0
     */
    public void SQLCo() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        // TODO code application logic here
        try{
            Class.forName("org.sqlite.JDBC");
           // Connection cnx = DriverManager.getConnection("jdbc:sqlite:/media/pi/ESD-USB/DEMO/test_db/partie.sqlite");
             Connection cnx = DriverManager.getConnection("jdbc:sqlite:partie.sqlite");

            link = cnx.createStatement();
            link2 = cnx.createStatement();
            System.out.println("Connection réussie");
            co = true;
        }catch(ClassNotFoundException |SQLException e){
            System.out.println(e);
            co=  false;
        }
        
    }

    /**
     * Verifie la connection à la base
     * 
     * @see BaseDeDonnees#co
     * 
     * @return la variable co de connection à la nase
     * 
     * @since 1.0
     *     
     */
    public boolean isCo() {
        return co;
    }
        
    /**
     * Insert la commande mise en paramètre dans la table
     * 
     * @return 
     * @see BaseDeDonnees#link
     * execute(java.lang.String) 
     * @see String
     * @see int
     * 
     * @param commande  type String qui est la commande à saisir
     * 
     * @throws SQLException On evite les erreur SQL
     * 
     * @since 1.0
     */
    public int Insert(String commande) throws SQLException{
        try{
            return link.executeUpdate(commande);
        }catch(SQLException e){
            System.out.println(e);
            return -1;
        }
             

    }
    
     /**
     * Select la commande mise en paramètre dans la table
     * 
     * @see BaseDeDonnees#link
     * @see Statement#executeQuery(java.lang.String) 
     *
     * 
     * @param commande  type String qui est la commande à saisir
     * 
     * @return On return les données récupérées dans la table
     * 
     * @throws SQLException On evite les erreur SQL
     * 
     * @since 1.0
     */
    
    public ResultSet Select(String commande) throws SQLException{
        try{
            return  link.executeQuery(commande); 
        }catch(SQLException e){
            return null;
        }
    }

}
