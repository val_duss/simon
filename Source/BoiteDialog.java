/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisi;

/**
 * Boite de Dialogue 
 *       Création de la boite de dialogue pour la fin des MDJ
 *
 * @author Valentin DUSSERVAIX
 * 
 * @version 1.0
 */
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class BoiteDialog extends JDialog implements ActionListener{
    /**
     * bouton Valider
     * 
     * @see JButton
     * @since 1.0
     * 
     */
    JButton btValider;
    /**
     * Label Label
     * 
     * @see JLabel
     * @since 1.0
     * 
     */
    JLabel Label;
    /**
     * Label Nom
     * 
     * @see JLabel
     * @since 1.0
     * 
     */
    JLabel Nom;
    /**
     * TextField Nom_
     * 
     * @since 1.0
     * 
     */
    JTextField Nom_;
    /**
     * String Text
     * 
     * @see String
     * @since 1.0
     * 
     */

    String text;


    /**
     * Constructeur de la boite de Dialog et création de la boite 
     * 
     * @see JLabel
     * @see String
     * @see JButton
     * @see JButton#addActionListener(java.awt.event.ActionListener) 
     * @see JTextField
     * @see JPanel
     * @see JPanel#add(java.awt.Component, java.lang.Object) 
     * @see JPanel#setLayout(java.awt.LayoutManager) 
     * @see GridBagConstraints
     * @see GridBagConstraints#BOTH
     * @see GridBagConstraints#fill
     * @see GridBagConstraints#gridwidth
     * @see GridBagConstraints#gridx
     * @see GridBagConstraints#gridy
     * @see JDialog#setContentPane(java.awt.Container) 
     * @see JDialog#pack() 
     * @see BoiteDialog#btValider
     * @see BoiteDialog#Label
     * @see BoiteDialog#Nom
     * @see BoiteDialog#Nom_
     * 
     * @param owner la fenetre sur la quelle on construit la boite
     * @param label la valeur que l'on veut mettre dans le label
     * 
     * @since 1.0
     */
    public BoiteDialog(Fenetre owner, String label) {
        super(owner,true); //constructeur de la classe MÃ¨re: owner =
             
        Label=new JLabel(label);
        Nom=new JLabel("Votre Pseudo");
        
        Nom_=new JTextField();
        btValider =new JButton("ok");
        btValider.addActionListener(this);
        
        JPanel pano = new JPanel();
        pano.setLayout(new GridBagLayout());
        GridBagConstraints cont  = new  GridBagConstraints();
        
        
        cont.fill = GridBagConstraints.BOTH;
        cont.gridwidth = 2;
        cont.gridx = 0;
        cont.gridy = 0;
        pano.add(Label, cont);
               
        
        
        cont.fill = GridBagConstraints.BOTH;
        cont.gridx = 0;
        cont.gridy = 1;
        pano.add(Nom, cont);
               
        cont.gridwidth = 4;
        cont.gridx = 1;
        cont.gridy = 2;
        pano.add(Nom_, cont);

        cont.gridx = 1;
        cont.gridy = 3;
        pano.add(btValider, cont);
        
        this.setContentPane(pano);
        this.pack();

    }
    
    
    /**
     * ActionListener, verification d'appui des boutons
     * 
     * @see ActionEvent
     * @see ActionEvent#getSource() 
     * @see BoiteDialog#btValider
     * @see BoiteDialog#setVisible(boolean) 
     * @see BoiteDialog#text
     * @see BoiteDialog#Nom_
     * @see JTextField#getText() 
     * 
     * @param e event l'action 
     * 
     * @since 1.0
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btValider){
            this.setVisible(false); //on ferme la fenÃªtre
            text = Nom_.getText();
        }

    }
    
    /**
     * Montre la boite de dialog
     * 
     * @see BoiteDialog#setVisible(boolean) 
     * @see BoiteDialog#text
     * 
     * @return text la valeur mise dans le text field
     * 
     * @since 1.0
     */
    public String showDialog(){
        this.setVisible(true); //on ouvre la fenÃªtre
        return text;
    }
}//fin de la classe
