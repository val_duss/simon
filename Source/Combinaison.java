/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisi;

import java.awt.Color;
import static java.lang.Thread.sleep;
import java.util.ArrayList;

/**
 * Classe Combinaison, liste toutes les couleurs de la combinaison du simon
 *@author Valentin Dusservaix
 * @version 1.0
 */
public class Combinaison {
   /**
    * @see ArrayList
    * @see Color
    * 
    * @since 1.0
    */
    private final ArrayList<Color> combinaison;
    /**
     * @see Color
     * 
     * @since 1.0
     */
    public Color couleur[] = new Color[4];
   
    /**
    * Constructeur de la Combinaison
    * 
    * @see ArrayList
    * @see Combinaison#combinaison
    * @see Combinaison#couleur
    * @see Color
    * 
    * @since 1.0
    */
  
    public Combinaison(){
        combinaison = new ArrayList<>();       
        couleur[0] = Color.RED;
        couleur[1] = Color.GREEN;
        couleur[2] = Color.BLUE;
        couleur[3] = Color.YELLOW;

    }
    
    /**
    * Comparateur de combinaison
    * 
     * @return 
    * @see boolean
    * @see Color
    * @see ArrayList#get(int) 
    * @see int
    * @see Combinaison#combinaison
    * @see Color#equals(java.lang.Object) 
    * 
    * @param c 
    *       La couleur d'entrée du program
    * @param id
    *       Le numero de la couleur dans l'array list à comparer
    * 
    * @since 1.0
    */
    public boolean equals (Color c, int id){
        return c == combinaison.get(id);
    }
    
    /**
    * Fonction d'affichage console des couleurs
    * etape 1 seulement dans la console
    * 
    * @see Thread#sleep(long) 
    * @see Combinaison#combinaison
    * @see ArrayList#size
    * @see ArrayList#get(int) 
    * 
    * @throws java.lang.InterruptedException
    * @since 1.0
    */
    public void affichage() throws InterruptedException{
        
            for(int i = 0; i<combinaison.size(); i++){
              sleep(600);
            System.out.println(combinaison.get(i));
            }

    }
    /**
     * Récupération de l'arraylist combinaison
     * 
     * @see ArrayList
     * @see Color
     * @see Combinaison#combinaison
     * 
     * @return combinaison, l'arraylist de toutes les couleur de la combinaison
     * 
     * @since 1.0
     */
    public ArrayList<Color> getCombinaison() {
        return combinaison;
    }
    
   /**
    * Récupération de la couleur a l'index i
    * 
    * @see Color
    * @see ArrayList
    * @see ArrayList#get(int) 
    * @see Combinaison#combinaison
    * 
    * @param i, index pour selection sa valeur dans l'arraylist
    * @return 
    * 
    * since 1.0
    */
     public Color affichage(int i) {
        
            return combinaison.get(i);
   
    }
    
     
    /**
    * Ajout une couleur a l'array list
    * 
     * @param c
    * @see Color
    * @see ArrayList#add(java.lang.Object) 
    * @see Combinaison#combinaison
    * 
    * @since 1.0
    */
    public void add(Color c){
        combinaison.add(c);
    }
    
    /**
    * Ajout une couleur a l'array list aléatoirement
    * 
    * @see Color
    * @see ArrayList
    * @see ArrayList#add(java.lang.Object) ) 
    * @see Combinaison#combinaison
    * @see Math#round(double) 
    * @see Math#random() 
    * 
    * @since 1.0
    */
    public void add(){

       combinaison.add(couleur[(int)Math.round(Math.random()*3)]);
    }
    
    
}
