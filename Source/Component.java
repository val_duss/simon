/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisi;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;

/**
 *  Classe Component création des composant pour l'interface
 * 
 * @author Mathieu DUBAYLE
 * @version 1.1
 */
public class Component extends JComponent {
    /**
     * @see int
     * 
     * @since 1.0
     */
    private final int type;
    /**
     * @see int
     * 
     * @since 1.0
     */
    private final int letterHeigth=50;
    
    /**
     * Constructeur du composant
     * 
     * @see Component#type
     * 
     * @param type, int type permet de selectionner le type de composent a créer
     * 
     * @since 1.0
     */
    public Component(int type){
        this.type = type;
    }
    
    
    /**
     * Mide en Place du fond application
     * 
     *      
     * @param g dessin
     * @param brightness 
     * @since 1.1
     */
    public void background(Graphics g,int brightness){
        
    }
    /**
     * Affichage du titre
     * 
     * @see Component#type
     * @see Graphics
     * @see Graphics#setColor(java.awt.Color) 
     * @see Graphics#drawString(java.lang.String, int, int) 
     * @see Color
     * 
     * 
     * @param g
     * @since 1.1
     * 
     */
    @Override
    public void paintComponent(Graphics g){
        if(type == 1){
            g.setColor(Color.BLUE);
            g.drawString((String)"S", letterHeigth, letterHeigth);
            g.setColor(Color.RED);
            g.drawString("I", letterHeigth, letterHeigth);
            g.setColor(Color.YELLOW);
            g.drawString("M",letterHeigth,letterHeigth);
            g.setColor(Color.GREEN);
            g.drawString("O",letterHeigth,letterHeigth);
            g.setColor(Color.ORANGE);
            g.drawString("N", letterHeigth, letterHeigth);
        }
        g.drawString("S", WIDTH, WIDTH);
    }
}
