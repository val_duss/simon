/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisi;

import java.awt.BorderLayout;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Class Fenetre de base gère l'interface IHM, La Base de donnée
 *
 * @author Mathieu DUBAYLE
 * @version 1.1
 */
public class Fenetre extends JFrame{


    /**
     *  
     * @see JPanel
     * 
     * @since 1.1
     */
     private final JPanel viewPanel;
     /**
      * 
      * @since 1.0
      */
     private final BaseDeDonnees BDD = new BaseDeDonnees();
     /**
      * @see Vector
      * @see String
      * 
      * @since 1.0
      */
      private final Vector<String> v = new Vector<>();
    
    /**
     * Constructeur de Fenetre
     * 
     * 
     * @see Fenetre#setDefaultCloseOperation(int) 
     * @see Fenetre#setSize(java.awt.Dimension) 
     * @see Fenetre#viewPanel
     * @see Fenetre#add(java.awt.Component, java.lang.Object) 
     * @see JPanel
     * @see BorderLayout
     * @see Fenetre#BDD
     * @see BaseDeDonnees#SQLCo() 
     * @see Fenetre#showView(javax.swing.JPanel) 
     * 
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException 
     * 
     * @since 1.0
     */
    public Fenetre() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(720, 480);
        viewPanel = new JPanel(new BorderLayout());
        add(viewPanel, BorderLayout.CENTER);
        BDD.SQLCo();
        showView(new Menu(this));
       
        
    }

    /**
     * Recupération du panel view
     * 
     * @see Fenetre#viewPanel
     *  
     * @return viewPanel, le panel a changer 
     * 
     * @since 1.0
     */
    public JPanel getViewPanel() {
        return viewPanel;
    }


    /**
     * Récupération de Base de données
     * @see Fenetre#BDD
     * @see BaseDeDonnees
     * 
     * @return BDD, la base de de données du programme
     * 
     * @since 1.0
     */
    public BaseDeDonnees getBDD() {
        return BDD;
    }
    
    /**
     * Récupération du vecteur 
     * 
     * @see Vector
     * @see Fenetre#v
     * @see String
     * 
     * 
     * @return v, le vecteur remplacant la base de données
     * @since 1.0
     */
    public Vector<String> getV() {
        return v;
    }
    
    


    
    /**
     * Fonction de réatribution du panel
     * 
     * @see JPanel
     * @see Fenetre#viewPanel
     * @see Fenetre#setContentPane(java.awt.Container) 
     * @see JPanel#revalidate() 
     * @see JPanel#repaint() 
     * 
     * @param panel, le panel de remplacement
     * 
     * @since 1.0
     */
     public void showView(JPanel panel) {
        viewPanel.removeAll();
        this.setContentPane(panel);
        panel.revalidate();
        panel.repaint();
   }
    
}

