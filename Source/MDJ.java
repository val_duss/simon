/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisi;

import javax.swing.JPanel;

/**
 *  Classe mère des mode de jeu
 * 
 * @author Valentin DUSSERVAIX
 * 
 * @version 1.0
 */
public abstract class MDJ extends JPanel{
    
    /**
     * fonction abstract deroulement
     * 
     * @see boolean
     * 
     * @return boolean, si la partie est terminé ou non
     * 
     * @since 1.0
     */
    public abstract boolean deroulement();
    /**
     * fonction abstract deroulement
     * 
     * @see String
     * 
     * @return String, le nom du mode de jeu
     * 
     * @since 1.0
     */
    public abstract String getNom();
    
    /**
     * fonction abstract deroulement
     * 
     * @see int
     * 
     * @return int, le score de la partie
     * 
     * @since 1.0
     */
    public abstract int getScore();
    
    /**
     * fonction abstract deroulement
     * 
     * @see String
     * 
     * @return String, le nom du joueur de la partie
     * 
     * @since 1.0
     */
    public abstract String getJoueur();
    
    /**
     * fonction abstract deroulement
     * 
     * @see JPanel
     * 
     * @return JPanel, le panel du mode de jeu
     * 
     * @since 1.0
     */
     public abstract JPanel createGUI();
}

