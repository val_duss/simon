/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisi;

import java.awt.Color;
import java.awt.Font;
import static java.lang.Thread.sleep;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 * Mode de jeu Lucky
 * 
 * @author Valentin DUSSERVAIX, mathieu DUBAYLE
 * 
 * @version 1.0
 */
public class MDJ_Lucky extends MDJ {
    /**
     * @see JLabel
     * 
     * @since 1.0
     */
    private JLabel rouge;
    /**
     * @see JLabel
     * 
     * @since 1.0
     */
    private JLabel vert;
    /**
     * @see JLabel
     * 
     * @since 1.0
     */
    private JLabel bleu;
    /**
     * @see JLabel
     * 
     * @since 1.0
     */
    private JLabel jaune;
    /**
     * @see int
     * 
     * 
     * @since 1.0
     */
    volatile int pan = -1;
    /**
     * @see Fenetre
     * 
     * @since 1.0
     */
    final private Fenetre fenetre;
    /**
     * 
     * 
     * @since 1.0
     */
    private final int score;
    /**
     * @see String
     * 
     * @since 1.0
     */
    private String joueur;
    /**
     * 
     *  @since 1.0
     */
    private int vie[] = new int[4];
    /**
     * @see Thread
     * 
     * @since 1.0
     */
    private final Thread t = new Thread() {
            /**
             * Runnable
             * 
             * @see MDJ_Lucky#deroulement() 
             * @see Thread#currentThread() 
             * @see Thread#interrupt() 
             * @see SwingUtilities#invokeLater(java.lang.Runnable) 
             * @see MDJ_Lucky#fenetre
             * @see Menu
             * @see Fenetre#showView(javax.swing.JPanel) 
             * 
             * @since 1.0
             */
            @Override
            public void run() {
                if (!deroulement()) {
                    Thread.currentThread().interrupt();
                    SwingUtilities.invokeLater(() -> {
                        try {
                            fenetre.showView(new Menu(fenetre));
                        } catch (SQLException ex) {
                            Logger.getLogger(MDJ_test.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
                }

            }
        };


    /**
     * Constructeur de partie
     * 
     * @see MDJ_Lucky#vie
     * @see MDJ_Lucky#score
     * @see MDJ_Lucky#fenetre
     * 
     * @param fenetre, initalisation de la fenetre
     */
    public MDJ_Lucky(Fenetre fenetre) {
        super();
        this.fenetre = fenetre;
        score = 0;
        vie[0] = 3;
        vie[1] = 3;
        vie[2] = 3;
        vie[3] = 3;
    }
    
    /**
     * Fonction déroulement du mode de jeu
     * 
     * @see MDJ_Lucky#Bout
     * @see Thread#start() 
     * @see MDJ_Lucky#rouge
     * @see MDJ_Lucky#vert
     * @see MDJ_Lucky#bleu
     * @see MDJ_Lucky#jaune
     * @see MDJ_Lucky#joueur
     * @see MDJ_Lucky#fenetre
     * @see Font
     * @see JTextPane
     * @see JTextPane#setBounds(java.awt.Rectangle) 
     * @see JTextPane#setBackground(java.awt.Color) 
     * @see JTextPane#setFont(java.awt.Font) 
     * @see JTextPane#setText(java.lang.String) 
     * @see JLabel#setText(java.lang.String) 
     * @see Fenetre#getInte() 
     * @see Interface#EteindreB()
     * @see Interface#EteindreJ() 
     * @see Interface#EteindreR() 
     * @see Interface#EteindreV()  
     * @see Interface#AllumLEDB() 
     * @see Interface#AllumLEDJ() 
     * @see Interface#AllumLEDR() 
     * @see Interface#AllumLEDV() 
     * @see MDJ_Lucky#appendToPane
     * @see Delay#milliseconds(int) 
     * @see Thread#interrupt() 
     * 
     * @return boolean, si le deroulement est fini
     * 
     * @since 1.0
     */
    @Override
    public boolean deroulement() {
        Font fontText = new Font(Font.SANS_SERIF, 15, 15);

        JTextPane ready = new JTextPane();
        ready.setBounds(fenetre.getWidth() / 3 - 50, fenetre.getHeight() / 3, 300, 100);
        ready.setBackground(Color.DARK_GRAY);
        ready.setFont(fontText);

        int test = 4;
        while (test > 1) {
            
            rouge.setText("Joueur 1| vie: " + vie[0]);
            vert.setText("Joueur 2| vie:" + vie[1]);
            bleu.setText("Joueur 3| vie:" + vie[2]);
            jaune.setText("Joueur 4| vie:" + vie[3]);

            pan = -2;
            add(ready);
            appendToPane(ready, "READY ... \n", Color.CYAN);
            try {
                sleep(getRandom()*2);
            } catch (InterruptedException ex) {
                Logger.getLogger(MDJ_Lucky.class.getName()).log(Level.SEVERE, null, ex);
            }
            appendToPane(ready, "STEADY .... \n", Color.CYAN);
            try {
                sleep(getRandom()*2);
            } catch (InterruptedException ex) {
                Logger.getLogger(MDJ_Lucky.class.getName()).log(Level.SEVERE, null, ex);
            }
            appendToPane(ready, "GOOO !!!!!!!  \n", Color.CYAN);           
            
            if (pan != -3) {
                pan = -1;
                while (pan == -1) {

                }
                for (int i = 0; i < 4; i++) {
                    if (i != pan) {
                        vie[i]--;
                        if (vie[i] < 0) {
                            vie[i] = 0;
                        }
                    }
                }
                appendToPane(ready, "Le joueur " + (pan + 1) +"  A tiré sur tout le monde", Color.CYAN);
            } else {
                appendToPane(ready, "Un joueur a tiré trop vite", Color.CYAN);
            }

            test = 4;
            for (int i = 0; i < 4; i++) {
                if (vie[i] <= 0) {
                    test--;
                }
            }
            try {
                sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(MDJ_Lucky.class.getName()).log(Level.SEVERE, null, ex);
            }
            ready.setText("");
           

        }
        for (int i = 0; i < 4; i++) {
            if (vie[i] > 0) {
                appendToPane(ready, "Le joueur " + (pan + 1) + "A GAGNE!!", Color.CYAN);
                joueur = "Le joueur " + (pan + 1);
            }

        }


        
        
        return false;
    }

    /**
     * Recuperation du Nom
     * 
     * @see String
     * 
     * @return le nom
     * 
     * @since 1.0
     */
    @Override
    public String getNom() {
        return "Lucky Luck";
    }
    
    /**
     * Récupération du score
     * 
     *  
     * 
     * @return rien
     * @since 1.0
     */
    @Override
    public int getScore() {
        return 0;
    }
    
    /**
     * Retourn le nom du gagnant
     * 
     * @see String
     * @see MDJ_Lucky#joueur
     * 
     * @return joueur
     * @since 1.0
     */
    @Override
    public String getJoueur() {
        return joueur;
    }

    /**
     * Création des graphisme du mode de jeu
     * 
     * @see MDJ_Lucky#setBackground(java.awt.Color) 
     * @see MDJ_Lucky#rouge
     * @see MDJ_Lucky#vert
     * @see MDJ_Lucky#bleu
     * @see MDJ_Lucky#jaune
     * @see MDJ_Lucky#vie
     * @see MDJ_Lucky#add(java.awt.Component) 
     * @see Thread#start() 
     * 
     * @return JPanel du mode de jeu
     * @since 1.0
     */
    @Override
    public JPanel createGUI() {
        this.setBackground(Color.DARK_GRAY);
        //new FlowLayout(FlowLayout.CENTER);
        //GridBagConstraints uiCont = new GridBagConstraints();        
        setLayout(null);

        rouge = new JLabel("Joueur 1" + vie[0]);
        vert = new JLabel("Joueur 2" + vie[1]);
        bleu = new JLabel("Joueur 3" + vie[2]);
        jaune = new JLabel("Joueur 4" + vie[3]);

        add(rouge);
        add(vert);
        add(bleu);
        add(jaune);
        rouge.setBounds(0, fenetre.getHeight() / 4, 200, fenetre.getHeight() / 2);
        rouge.setBackground(Color.RED);
        rouge.setForeground(Color.red);

        vert.setBounds(fenetre.getWidth() - 200, fenetre.getHeight() / 4, 200, fenetre.getHeight() / 2);
        vert.setBackground(Color.GREEN);
        vert.setForeground(Color.GREEN);

        bleu.setBounds(fenetre.getWidth() / 4, fenetre.getHeight() - 90, fenetre.getWidth() / 2, 100);
        bleu.setBackground(Color.BLUE);
        bleu.setForeground(Color.BLUE);

        jaune.setBounds(fenetre.getWidth() / 4, 0, fenetre.getWidth() / 2, 50);
        jaune.setBackground(Color.YELLOW);
        jaune.setForeground(Color.YELLOW);
        t.start();

        return this;
    }

    /**
     * Ajoute a un JTextPane un message d'une couleur définit
     *
     * @see JTextPane
     * @see JTextPane#getDocument()
     * @see JTextPane#replaceSelection(java.lang.String) 
     * @see JTextPane#setCaretPosition(int) 
     * @see JTextPane#setCharacterAttributes(javax.swing.text.AttributeSet, boolean) 
     * @see StyleContext
     * @see StyleContext#getDefaultStyleContext() 
     * @see StyleContext#addAttribute(javax.swing.text.AttributeSet, java.lang.Object, java.lang.Object) 
     * AttributeSet
     * 
     * @param tp ,  TextPane a modifier
     * @param msg , String a insérer
     * @param c , couleur du text
     * 
     * @since 1.2
     */
    private void appendToPane(JTextPane tp, String msg, Color c) {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }
    
    
    /**
     * Recup un random
     * 
     * @see Math#random() 
     * 
     * 
     * @return retourne un random
     * 
     * @since 1.0
     */
    private int getRandom(){
        //int tab[]={500,200,300,400,500,600,700,800,900,1000};
        int x=(int)(Math.random()*100);
        
        if (x>=0 && x<10) {
            System.out.println("500");
            return 500;
        }
        if (x>=10 && x<20) {
            System.out.println("250");
            return 250;
        }
        if (x>=20 && x<30) {
            System.out.println("750");
            return 750;
        }
        if (x>=30 && x<40) {
            System.out.println("600");
            return 600;
        }
        if (x>=40 && x<50) {
            System.out.println("1100");
            return 1100;
        }
        if (x>=50 && x<60) {
            System.out.println("800");
            return 800;
        }
        if (x>=60 && x<70) {
            System.out.println("900");
            return 900;
        }
        if (x>=70 && x<80) {
            System.out.println("1300");
            return 1300;
        }
        if (x>=80 && x<90) {
            System.out.println("1600");
            return 1600;
        }
        if (x>=90 && x<100) {
            System.out.println("300");
            return 300;
        }
        System.out.println("300");
        return 0;
    }

}
