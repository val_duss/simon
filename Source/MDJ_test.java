/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisi;

import java.awt.Color;
import static java.lang.Thread.sleep;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
 

/**
 * MDJ_test class du mode de jeu test
 *
 * @author Valentin Dusservaix, Mathieu DUBAYLE
 * @version 1.0
 */
public class MDJ_test extends MDJ{
    /**
     * Color couleur volatile accessible depuis toutes les fonctions 
     * 
     * @see Color
     * @since 1.0
     */
    volatile private Color type = Color.BLACK;
    /**
     * Fenetre fenetre
     *
     * @see Fenetre
     * @since 1.0
     */
    final private Fenetre fenetre;
    /**
     * Int Score
     * 
     * @see int
     * @since 1.0
     */
    volatile private int score;
    /**
     * String Joueur
     * 
     * @see String
     * @since 1.0
     */
    volatile private String joueur;
    /**
     * Thread t
     * 
     * @see Thread
     * @since 1.0
     */
    private final Thread t = new Thread() {
            /**
             * Run boucle runnable qui check l'appui des boutons
             * 
             * @see MDJ_test#deroulement
             * @see Menu
             * @see MDJ_test#fenetre
             * @see SwingUtilities#invokeLater(java.lang.Runnable) 
             * @see Fenetre#showView(javax.swing.JPanel) 
             * @since 1.0
             */
            @Override
            public void run() {
                // Attente de la fin du déroulement pour changer de fenetre
                if (!deroulement()) {
                    //Arrêt du thread
                     
                    Thread.currentThread().interrupt();
                    // Essaye une ouverture de la fenetre menu
                    
                    SwingUtilities.invokeLater(() -> {
                        try {
                            fenetre.showView(new Menu(fenetre));
                        } catch (SQLException ex) {
                            Logger.getLogger(MDJ_test.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
                }

            }
        };
    /**
     * Constructeur de MDJ_test
     *      attribu le score a 0 et la fenetre mise en parametre
     * 
     * @param fenetre Prend en paramètre la fenetre
     * 
     * @see MDJ_test#Bout
     * @see MDJ_test#score
     * @see MDJ_test#fenetre
     * @since 1.0
     */
    public MDJ_test(Fenetre fenetre) {
        super();
        /**
         * Initialisation du thread bouton
         */
        
        this.fenetre = fenetre;
        score = 0;
    }

    /**
     * Fonction déroulement Override
     *      Le déroulement du mode de jeu
     * 
     * @return Un boolean pour savoir si le mode de jeu est terminé ou non
     * 
     * @see MDJ_test#type
     * @see MDJ_test#score
     * @see MDJ_test#joueur
     * @see MDJ_test#Bout
     * @see Combinaison
     * @see Combinaison#add(java.awt.Color) 
     * @see Combinaison#getCombinaison() 
     * @see Combinaison#affichage(int) 
     * @see Combinaison#equals(java.awt.Color, int)
     * @see boolean
     * @see JPanel#setBackground(java.awt.Color) 
     * @see JPanel#repaint() 
     * @see Fenetre#getInte
     * @see Interface#Allum(java.awt.Color) 
     * @see Thread#interrupt() 
     * @see Color
     * @see BoiteDialog
     * @see BoiteDialog#showDialog() 
     * @since 1.0
     */
    @Override
    public boolean deroulement() {
        // type prend la couleur noir
        type = Color.BLACK;
        // score prend la valeur 0
        score = 0;
        //joueur est null
        joueur = null;
        // On créer une nouvelle combinaison
        Combinaison combi = new Combinaison();
        //On créer un varaible test qui prend la valeur true
        boolean test = true;
        //début de la boucle de jeu
        while (test) {
            // Ajout d'une couleur dans la combinaison
            combi.add();
            // Affichage des couleur présente dans la combinaison
            for (int i = 0; i < combi.getCombinaison().size(); i++) {
                //Affichage graphique du background
                this.setBackground(combi.affichage(i));
                this.repaint();
         
                try {
                    // attente entre chaque affichage
                    sleep(600);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MDJ_test.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                this.setBackground(Color.BLACK);
                this.repaint();
                 try {
                    // attente entre chaque affichage
                    sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MDJ_test.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //Boucle de comparaison des affichages avec l'entrée du joueur
             
            for (int i = 0; i < (score + 1) && test == true; i++) {
                // attente du changement de la couleur par les boutons
                 
                while (type == Color.BLACK) {

                }
                // test de la combinaison si fausse arrêt des boucles
                test = combi.equals(type, i);
                type = Color.BLACK;
                if (true) {
                    System.out.println("Juste");
                } else {
                    System.out.println("Combinaison fausse");

                }
            }
            // Incrémentation du score
             
            score++;
            System.out.println("score :" + score);
			 try {
                    // attente entre chaque affichage
                    sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MDJ_test.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        // ouverture de la boite de dialog pour la fin de la partie
        BoiteDialog boite = new BoiteDialog(fenetre, "PERDU!!! votre score est de :" + score);
        // Récupération de la variable joueur
        joueur = boite.showDialog();
        System.out.println("PERDU!!! votre score est de :" + score);//retourner le score
        //Arrêt du check des boutons
        t.interrupt();
        System.out.println("fin de la partie");
        if (fenetre.getBDD().isCo()) {
            try {
             fenetre.getBDD().SQLCo();
            int val = fenetre.getBDD().Insert("insert into partie(mdj , score , joueur) values (\""+getNom()+"\","+ score+ ", \""+joueur+"\")");        
              System.out.println("Insertion: "+val);
            } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(MDJ_test.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Insertion");
        } else {
            fenetre.getV().add(getNom() + "|" + getJoueur() + "|Score: " + getScore());
        }
                
        // Arrêt de la partie
        return false;
    }
    
    /**
     * Récupération de la valeur du Nom
     * 
     * @see String
     * 
     * @return le nom du mode de jeu
     * @since 1.0
     */
    @Override
    public String getNom() {
        return "Mode de jeu simple";
    }

    /**
     * Création de l'affichage du mode de jeu
     * 
     * @see JButton
     * @see JPanel#setLayout(java.awt.LayoutManager) 
     * @see JPanel#add(java.awt.Component) 
     * @see JButton#setBounds(java.awt.Rectangle) 
     * @see JButton#setBackground(java.awt.Color) 
     * @see JButton#setBorderPainted(boolean) 
     * @see JButton#addActionListener(java.awt.event.ActionListener) 
     * @see MDJ_test#t
     * @see Thread#start() 
     * 
     * @return JPanel, le panel d'affichage du mode de jeu
     * @since 1.0
     */
    @Override
    public JPanel createGUI() {

        setLayout(null);
        
        // Attribution des boutons
        JButton rouge = new JButton();
        JButton vert = new JButton();
        JButton bleu = new JButton();
        JButton jaune = new JButton();
        
        //ajout au panel
        add(rouge);
        add(vert);
        add(bleu);
        add(jaune);

        // Affichage de ces boutons dans le panel
        rouge.setBounds(0, fenetre.getHeight()/4, 70, fenetre.getHeight()/2);
        rouge.setBackground(Color.RED);
        rouge.setBorderPainted(false);
        
        vert.setBounds(fenetre.getWidth()-80, fenetre.getHeight()/4, 80, fenetre.getHeight()/2);
        vert.setBackground(Color.GREEN);
        vert.setBorderPainted(false);
        
        bleu.setBounds(fenetre.getWidth()/4,fenetre.getHeight()-90,fenetre.getWidth()/2,100);
        bleu.setBackground(Color.BLUE);
        bleu.setBorderPainted(false);        
        
        jaune.setBounds(fenetre.getWidth()/4,0,fenetre.getWidth()/2,50);
        jaune.setBackground(Color.YELLOW);
        jaune.setBorderPainted(false);

        //Ajout des actionListener entrée des bouton au clic
        rouge.addActionListener(event -> {
            // Affiche la valeur du bouton
            System.out.println("rouge");
            // Change la variable type par rapport à la valeur du bouton
            type = Color.RED;
            // Repaint  le panel
            this.repaint();
        });
        vert.addActionListener(event -> {
            System.out.println("vert");
            type = Color.GREEN;
            this.repaint();
        });
        bleu.addActionListener(event -> {
            System.out.println("bleu");
            type = Color.BLUE;
            this.repaint();
        });
        jaune.addActionListener(event -> {
            System.out.println("jaune");
            type = Color.YELLOW;
            this.repaint();
        });
        //lance le thread du déroulement
        t.start();

        return this;
    }
    /**
     * Récupération du score
     *    
     * @see int
     * @see MDJ_test#score
     * 
     * @return int score, le score du mode de jeu 
     * 
     * @since 1.0
     */
    @Override
    public int getScore() {
        return score;
    }
    /**
     * Récupération du nom du joueur
     * 
     * @see String
     * @see MDJ_test#joueur
     * 
     * @return String Joueur, la valeur du nom du joueur de la partie
     * 
     * @since 1.0
     */
    @Override
    public String getJoueur() {
        return joueur;
    }

}
