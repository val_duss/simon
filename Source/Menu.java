/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisi;

import javax.swing.*;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 * Version 1.2
 * @author p1805243
 */
public class Menu extends JPanel {

    /**
     * bouton quitter application
     *
     * @since 1.1
     * @see JButton
     *
     */
    private final JButton quit = new JButton("QUITTER");
    /**
     * bouton démarrer partie
     *
     * @since 1.1
     * @see JButton
     *
     */
    private final JButton launch = new JButton("  JOUER  ");
    /**
     * affiche les noms des différents mode de jeu disponible
     *
     * @since 1.1
     *
     */
    private final JTextField gameMod = new JTextField() {
        /**
         * redéfinition de la fonction setBorder, ici elle est vide puisque ne
         * l'on n'en souhaite pas
         *
         * @param border
         */
        @Override
        public void setBorder(Border border) {

        }
    };
    /**
     * bouton qui change de mode de jeu, parcourt de la liste vers la gauche
     *
     * @since 1.1
     * @see JButton
     *
     */
    private final JButton chevronG = new JButton("<");
    /**
     * bouton qui change de mode de jeu, parcourt de la liste vers la droite
     *
     * @since 1.1
     * @see JButton
     *
     */
    private final JButton chevronD = new JButton(">");

    /**
     * liste de mode de jeu, implémente tout ses modes de jeu hérités
     *
     * @since 1.1
     * @see MDJ
     */
    private final ArrayList<MDJ> listMDJ = new ArrayList();

    /**
     * int permettant de donner la valeur courante de la liste de mode de jeu
     *
     * @since 1.1
     *
     */
    private int curValueList = 0;

    /**
     * JTextPane sert d'affichage pour le titre de l'application
     *
     * @since 1.1
     * @see JTextPane
     */
    private final JTextPane title = new JTextPane();
    /**
     * Fenetre, utilise la même que celle crée a l'initialisation du jeu
     *
     * @since 1.2
     * @see Fenetre
     */
    final private Fenetre fenetre;
    /**
     * JList, stock toutes les données de fin de jeu (string) Nom score MDJ
     *
     * @since 1.2
     */
    private JList jList = new JList<>();
    /**
     * Boolean d'activation de la rotation des LEDs
     * 
     * @see boolean
     * 
     * since 1.2
     */
    volatile private boolean led = true;

    /**
     * constructeur de la classe, initialise le menu principal du jeu
     *
     * @since 1.2
     * @param fenetre récupère la fenetre crée dans la classe fenetre pour
     * update
     *
     * @see Menu#createGUI()
     * @see Fenetre
     * @see Fenetre#getBDD()
     * @see BaseDeDonnees#isCo()
     * @see BaseDeDonnees#Select(java.lang.String) Permet d'ffectuer une requête
     * SQL sur la base
     * @see ResultSet Sert de buffer pour le retour de la requete
     * @see ResultSet#next()
     * @see ResultSet#getString(int)
     *
     * @throws SQLException Lance un échec d'accès à la base de données
     */
    public Menu(Fenetre fenetre) throws SQLException {
        super();

        this.fenetre = fenetre;
        if (fenetre.getBDD().isCo()) {
            fenetre.getV().removeAllElements();
            ResultSet rs = fenetre.getBDD().Select("Select * from partie  ORDER BY score DESC limit 5");
            if(rs != null){
                while (rs.next()) {
                this.fenetre.getV().add(rs.getString(1) + "|" + rs.getString(3) + "|Score: " + rs.getString(2));
                    System.out.println(rs.getString(1) + "|" + rs.getString(3) + "|Score: " + rs.getString(2));
                }
            }else{
                System.out.println("Pas de données");
            }
            
        }

        createGUI();
    }

    /**
     * Cree l'interface utilisateur avec les boutons, les titres et les zones
     * d'affichage
     *
     * @since 1.2
     *
     * @see Menu#gameLaunch()
     * @see Menu#appendToPane(javax.swing.JTextPane, java.lang.String,
     * java.awt.Color)
     * @see Menu#curValueList
     * @see Menu#listMDJ
     *
     * @see JPanel#setLayout(java.awt.LayoutManager)
     * @see JPanel#add(java.awt.Component)
     * @see JPanel#setSize(int, int)
     *
     * @see JTextPane
     * @see JTextPane#setFont(java.awt.Font)
     * @see JTextPane#setBackground(java.awt.Color)
     * @see JTextPane#setEditable(boolean)
     *
     * @see JButton#setFont(java.awt.Font)
     * @see JButton#setBackground(java.awt.Color)
     * @see JButton#borderPainted(boolean)
     * @see JButton#setForeground(java.awt.Color)
     *
     * @see GridBagConstraints#gridx
     * @see GridBagConstraints#gridy
     * @see GridBagConstraints#gridwidht
     * @see GridBagConstraints#gridheight
     *
     * @see Font
     *
     *
     */
    private void createGUI() {
       
        setLayout(new FlowLayout());
        this.setBackground(Color.DARK_GRAY);
        this.setLayout(new GridBagLayout());
        GridBagConstraints cont = new GridBagConstraints();
        //cont.fill=GridBagConstraints.HORIZONTAL;

        //Ajout des différents mode de jeu disponible a listMDJ
        listMDJ.add(new MDJ_test(fenetre));
     
        listMDJ.add(new MDJ_Hard(fenetre));
        listMDJ.add(new MDJ_Lucky(fenetre));
        
        //Ajoute le JTextPane au panelMenu
        cont.gridx = 0;
        cont.gridy = 0;
        cont.gridwidth = 0;
        this.add(title, cont);

        //Change la police de charactère du JtextPane
        Font fontTitle = new Font(Font.SANS_SERIF, 60, 60);
        title.setFont(fontTitle);

        //Change la couleur de fond du JTextPane
        title.setBackground(Color.DARK_GRAY);

        //Ajout des lettre colorées au JTextPane 
        appendToPane(title, "S", Color.CYAN);
        appendToPane(title, "I", Color.RED);
        appendToPane(title, "M", Color.YELLOW);
        appendToPane(title, "O", Color.GREEN);
        appendToPane(title, "N", Color.ORANGE);

        //Empêche l'édition du JTextPane
        title.setEditable(false);

        //Police pour les JButton
        Font fontButton = new Font(Font.SANS_SERIF, 30, 30);

        //Police pour le JTextField
        Font gameModFont = new Font(Font.SANS_SERIF, 20, 20);

        //Ajout du bouton lancer au panelMenu
        cont.gridx = 0;
        cont.gridy = 1;
        cont.gridwidth = 0;
        this.add(launch, cont);

        launch.addActionListener(event -> {
            System.out.println("launch");
            try {
                this.gameLaunch();
            } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        /*
        Paramétrage du JButton launch :
            couleur de fond gris foncé
            suppression des bordures
            changement de la police
            couleur du texte jaune     
         */

        launch.setBackground(Color.DARK_GRAY);
        launch.setBorderPainted(false);
        launch.setFont(fontButton);
        launch.setForeground(Color.getHSBColor((float) 0.15, (float) 1.0, (float) 1.0));

        //Ajout d'un JButton chevron orienté à gauche
        cont.gridx = 0;
        cont.gridy = 2;
        cont.gridwidth = 2;
        this.add(chevronG, cont);

        chevronG.addActionListener((ActionEvent event) -> {
            System.out.println("chevronG");
            if (curValueList == 0) {
                curValueList = listMDJ.size() - 1;
                gameMod.setText(listMDJ.get(curValueList).getNom());
            } else {
                curValueList--;
            }
            gameMod.setText(listMDJ.get(curValueList).getNom());
        });

        /*
        Paramétrage du JButton chevronG :
            couleur de fond gris foncé
            suppression des bordures
            changement de la police
            couleur du texte rouge             
         */
        chevronG.setBackground(Color.DARK_GRAY);
        chevronG.setBorderPainted(false);
        chevronG.setFont(fontButton);
        chevronG.setForeground(Color.getHSBColor((float) 0.0, (float) 1.0, (float) 1.0));

        //Ajout d'un JTextField au panelMenu
        cont.gridx = 2;
        cont.gridy = 2;
        cont.gridwidth = 3;
        this.add(gameMod, cont);

        /*
        Paramétrage du JTextField gameMod :
            couleur de fond gris foncé
            suppression des bordures
            changement de la police
            couleur du texte blanc              
         */
        gameMod.setHorizontalAlignment(SwingConstants.CENTER);
        gameMod.setText(listMDJ.get(0).getNom());
        gameMod.setBackground(Color.DARK_GRAY);
        gameMod.setEditable(false);
        gameMod.setFont(gameModFont);
        gameMod.setForeground(Color.white);

        //Ajout du JButton chevronD au panelMenu
        cont.gridx = 5;
        cont.gridy = 2;
        cont.gridwidth = 2;
        this.add(chevronD, cont);

        chevronD.addActionListener(event -> {
            System.out.println("chevronD");
            if (curValueList == listMDJ.size() - 1) {
                curValueList = 0;
                gameMod.setText(listMDJ.get(curValueList).getNom());
            } else {
                curValueList++;
            }
            gameMod.setText(listMDJ.get(curValueList).getNom());

        });
        /*
        Paramétrage du bouton :
            couleur de fond gris foncé
            suppression des bordures
            changement de la police
            couleur du texte vert             
         */
        chevronD.setBackground(Color.DARK_GRAY);
        chevronD.setBorderPainted(false);
        chevronD.setFont(fontButton);
        chevronD.setForeground(Color.getHSBColor((float) 0.4, (float) 1.0, (float) 1.0));

        //Ajout du JButton quit au panelMenu
        cont.gridx = 0;
        cont.gridy = 3;
        cont.gridwidth = 0;
        this.add(quit, cont);

        quit.addActionListener(event -> {
            System.exit(0);

        });
        /*
        Paramétrage du bouton :
            couleur de fond gris foncé
            suppression des bordures
            changemlent de la police
            couleur du texte blueue                     
         */

        quit.setBackground(Color.DARK_GRAY);
        quit.setBorderPainted(false);
        quit.setFont(fontButton);
        quit.setForeground(Color.getHSBColor((float) 0.55, (float) 1.0, (float) 1.0));

        jList = new JList(fenetre.getV());

        cont.gridx = 0;
        cont.gridy = 4;
        this.add(jList, cont);

        //drawing.repaint();
        // fenetre.pack();
        //title.addFocusListener();
    }

    /**
     * Ajoute a un JTextPane un message d'une couleur définit
     *
     * @see JTextPane
     * @see JTextPane#getDocument()
     * @see JTextPane#replaceSelection(java.lang.String) 
     * @see JTextPane#setCaretPosition(int) 
     * @see JTextPane#setCharacterAttributes(javax.swing.text.AttributeSet, boolean) 
     * @see StyleContext
     * @see StyleContext#getDefaultStyleContext() 
     * @see StyleContext#addAttribute(javax.swing.text.AttributeSet, java.lang.Object, java.lang.Object) 
     * AttributeSet
     * 
     * @param tp ,  TextPane a modifier
     * @param msg , String a insérer
     * @param c , couleur du text
     * 
     * @since 1.2
     */
    private void appendToPane(JTextPane tp, String msg, Color c) {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }

    /**
     * Lance la partie selon le mode de jeu définit dans l'interface
     * utilisateur, et envoie a la base de donnée les infos Nom Score
     *
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     * @since 1.2
     *
     * @see Partie#getMDJ()
     * @see Partie#getScore()
     * 
     * @see Fenetre#getBDD()
     * @see Fenetre#getV() 
     * @see Fenetre#showView(javax.swing.JPanel)
     *                                Permet de clear la fenetre actuelle
     *                                en passant le panel courant pour le
     *                                mettre a jour
     * 
     * @see BaseDeDonnees#Insert(java.lang.String) 
     *                                Permet de modifier la base de donnees
     * @see BaseDeDonnees#isCo()
     * @see MDJ#getNom()
     * @see MDJ#getJoueur() 
     * 
     * @see JPanel#repaint() 
     *
     * @throws SQLException
     */
    public void gameLaunch() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        led = false;

        Partie p = new Partie(listMDJ.get(curValueList), fenetre);
        System.out.println("fin de la partie");
//        if (fenetre.getBDD().isCo()) {
//            fenetre.getBDD().SQLCo();
//            //int val = fenetre.getBDD().Insert("insert into partie(mdj , score , joueur) values (\""+p.getMDJ().getNom()+"\","+ p.getScore()+", \""+p.getMDJ().getJoueur()+"\")");        
//            System.out.println("Insertion: "+val);
//        } else {
//            fenetre.getV().add(p.getMDJ().getNom() + "|" + p.getMDJ().getJoueur() + "|Score: " + p.getScore());
//        }

        fenetre.showView(this);
        this.repaint();
    }
}
