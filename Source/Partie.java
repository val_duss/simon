/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisi;


import javax.swing.SwingUtilities;
/**
 *  Classe Partie, gère les différentes partie du jeu
 * 
 * @author Valentin DUSSERVAIX
 * 
 * @version 1.0
 */
public class Partie {
    /**
     * @see MDJ
     * 
     * @since 1.0
     */
    private MDJ mode;
    /**
     * @see int
     * 
     * @since 1.0
     */
    private int score;
    /**
     * @see String
     * 
     * @since 1.0
     */
    private String joueur;
    
    /**
     * Constructeur de Partie
     * 
     * @see Partie#score
     * @see Partie#mode
     * @see Partie#joueur
     * @see Fenetre#showView(javax.swing.JPanel) 
     * @see MDJ#createGUI() 
     * @see MDJ#getJoueur() 
     * @see MDJ#getScore() 
     * @see SwingUtilities#invokeLater(java.lang.Runnable) 
     * 
     * @param m, mode de jeu de la partie
     * @param fenetre, fenetre du simon
     * 
     * @since 1.0
     */
    public Partie(MDJ m,Fenetre fenetre){
        score = 0;
        
        mode = m;
        System.out.println("Lancement mdj : " + m.getNom());
        SwingUtilities.invokeLater(() -> fenetre.showView( mode.createGUI()));
        score = mode.getScore();
        System.out.println(score+" "+mode.getScore());
        joueur = mode.getJoueur();
        System.out.println(joueur+" "+mode.getJoueur());
    }
    
    /**
     * Récupération du mode de jeu
     * 
     * @see Partie#mode
     * @see MDJ
     * 
     * @return mode, le mode de jeu de la partie
     * 
     * @since 1.0
     */
    public MDJ getMDJ(){
        return mode;
    }
    /**
     * Récupération du score
     * 
     * @see int
     * @see Partie#score
     * 
     * @return score, le score de la partie
     * 
     * @since 1.0
     */
    public int getScore() {
        return score;
    }
    /**
     * Attribution du score
     * 
     * @see Partie#score
     * @param score, le score 
     * 
     * @since 1.0
     */
    public void setScore(int score) {
        this.score = score;
    }
    /**
     * Définition de la chaine de caratère de la partie
     * 
     * @see Partie#mode
     * @see MDJ#getNom() 
     * @see MDJ#getScore() 
     * @see MDJ#getJoueur() 
     * 
     * @return la chaine de caractère correspondant à la partie
     * 
     * @since 1.0
     */
    public String toString(){
        return mode.getNom() + ", "+mode.getJoueur()+" Score:" +mode.getScore();
    }
}

