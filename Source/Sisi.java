/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisi;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 * Main du Programme
 * 
 * @author Valentin DUSSERVAIX
 * 
 * @version 1.1
 */
public class Sisi {

    /**
     * Main du Simon
     * 
     * 
     * @param args the command line arguments
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     * @throws java.sql.SQLException
     * 
     * @since 1.0
     */
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        /**
         * Thread principal d'appel de la fenetre
         * @see SwingUtilities#invokelater
         * 
         * @since 1.1
         */

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            /**
             * Fonction Runnable du thread
             * 
             * @see Fenetre
             * @see Fenetre#setVisible(boolean) 
             * 
             * since 1.1
             */
            @Override
            public void run() {
                try {
                    Fenetre screen = null;
                    screen = new Fenetre();
                    screen.setExtendedState(JFrame.MAXIMIZED_BOTH);
                    screen.setUndecorated(true);
                    screen.setVisible(true);
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
                    Logger.getLogger(Sisi.class.getName()).log(Level.SEVERE, null, ex);
                }
            } /**
             *
             */
        });
    }
    
}
